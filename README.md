# hello-docker-server

Tiny Docker-base web server

## Build Docker image

```shell
docker build --tag hello-docker-server .
```

with custom greeting:

```shell
docker build --tag hello-docker-server --build-arg TARGET='Code-Café' .
```

## Start server

```shell
docker run --detach --rm --name server --publish 1234:8080 hello-docker-server
```

## Stop server

```shell
docker stop --time 0 server
```
