FROM alpine
ARG TARGET=Thünen
ENV TARGET=$TARGET
COPY server.sh /
ENTRYPOINT /server.sh
