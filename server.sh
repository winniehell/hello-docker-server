#!/usr/bin/env sh

set -o errexit
set -o nounset

while true; do
  DATE=$(date)
  nc -vlp "${PORT:-8080}" << EOF
HTTP/1.1 200 OK
${DATE}
Content-Type: text/plain; charset=utf-8

Hello ${TARGET:-world}!
It's ${DATE}.
EOF
done
